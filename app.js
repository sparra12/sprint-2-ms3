const reasonInput= document.querySelector("#input-reason");
const amountInput= document.querySelector("#input-amount");
const confirmBtn= document.querySelector("#btn-add");
const cancelBtn= document.querySelector("#btn-clear");
const expensesList= document.querySelector("#expenses-list");
const totalExpensesOutput= document.querySelector("#total-expenses");
const alertCtrl= document.querySelector('ion-alert-controller');
const reasonInputIncome = document.querySelector("#input-reason-income");
const amountInputIncome = document.querySelector("#input-amount-income");
const totalIncomesOutput = document.querySelector("#total-incomes");
const confirmBtnIn= document.querySelector("#btn-add-income");
const cancelBtnIn= document.querySelector("#btn-clear-income");
const totalMoneyOutput = document.querySelector("#total-money");

let totalExpenses=0;
let totalIncomes=0;
let totalMoney=0;

const clear =() => 
{
    reasonInput.value="";
    amountInput.value="";
    reasonInputIncome.value="";
    amountInputIncome.value="";
}


confirmBtn.addEventListener('click', ()=>
{
    const enteredReason = reasonInput.value;
    const enteredAmount= amountInput.value;
    
    if(enteredReason.trim().length <=0||
    enteredAmount<= 0 || 
    enteredAmount.trim().length <=0)
    {
        //alert('invalid values!');
        alertCtrl.create({message: 'Please enter a valid reason and amount!', header:'Error', buttons:['Okay']}).then(alertElement => {alertElement.present();});
        return;
    }

    const newItem= document.createElement('ion-item');
    newItem.textContent = enteredReason + ': $-' + enteredAmount;
    
    expensesList.appendChild(newItem);
   
    totalExpenses -= +enteredAmount;
    totalExpensesOutput.textContent='$'+totalExpenses;
    totalMoney -= +enteredAmount;
    totalMoneyOutput.textContent='$'+totalMoney;
    clear();

});

confirmBtnIn.addEventListener('click', ()=>
{
    const enteredReasonInput = reasonInputIncome.value;
    const enteredAmountInput = amountInputIncome.value;

    if(enteredReasonInput.trim().length <=0||
    enteredAmountInput<= 0 || 
    enteredAmountInput.trim().length <=0)
    {
        //alert('invalid values!');
        alertCtrl.create({message: 'Please enter a valid reason and amount!', header:'Error', buttons:['Okay']}).then(alertElement => {alertElement.present();});
        return;
    }

    const newItemInput= document.createElement('ion-item');
    newItemInput.textContent = enteredReasonInput + ': $' + enteredAmountInput;

    expensesList.appendChild(newItemInput);
    
    totalIncomes += +enteredAmountInput;
    totalIncomesOutput.textContent='$'+totalIncomes;
    totalMoney += +enteredAmountInput;
    totalMoneyOutput.textContent='$'+totalMoney;
    clear();

});

cancelBtn.addEventListener('click', clear);
cancelBtnIn.addEventListener('click', clear);